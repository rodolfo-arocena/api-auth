package org.example.rest.controllers;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class AuthController {
    @PostMapping(path = "/auth", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = MediaType.APPLICATION_JSON_VALUE)
    public AuthResponses auth(@RequestParam Map<String, String> body) {
        AuthResponses authResponses = new AuthResponses();
        authResponses.setId("TestYobs");
        authResponses.setClassName("org.apereo.cas.authentication.principal.SimplePrincipal");
        authResponses.getAttributes().put("Name","Yobs 123");
        return authResponses;
    }
}
