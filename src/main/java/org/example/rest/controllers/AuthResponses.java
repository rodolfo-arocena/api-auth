package org.example.rest.controllers;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;

public class AuthResponses {
    private String id;
    @JsonProperty("@class")
    private String className;
    private HashMap<String, String> attributes = new HashMap<>();


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public HashMap<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(HashMap<String, String> attributes) {
        this.attributes = attributes;
    }
}
