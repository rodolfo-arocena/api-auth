package org.example.rest.controllerAdvice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.util.HashMap;
import java.util.Iterator;

@RestControllerAdvice
public class AuthExceptionHandling {

    Logger logger = LoggerFactory.getLogger(AuthExceptionHandling.class);


    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String  handleNoHandlerFound(NoHandlerFoundException e, WebRequest request) throws JsonProcessingException {
        HashMap<String, String> response = new HashMap<>();
        response.put("status", "fail");
        response.put("message", e.getLocalizedMessage());
        HashMap<String, String> headers = new HashMap<>();
        Iterator<String> headerNames = request.getHeaderNames();
        while(headerNames.hasNext()) {
            String headerName = headerNames.next();
            headers.put(headerName, request.getHeader(headerName));
        }

        ApiError apiError = new ApiError();
        if(request instanceof ServletWebRequest) {
            ServletWebRequest servletWebRequest =  ((ServletWebRequest)request);
            apiError.setUri(servletWebRequest.getRequest().getRequestURL().toString());
            apiError.setMethod(servletWebRequest.getRequest().getMethod());
            apiError.setHeaders(headers);
            //apiError.setBody(requestBody);
        }
        ObjectMapper objectMapper = new ObjectMapper();
        logger.error(objectMapper.writeValueAsString(apiError));
        return "Error";
    }
}
